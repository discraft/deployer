function checkFuel()
    if turtle.getFuelLevel() < 32 then
        turtle.select(1)
        turtle.placeUp()
        turtle.suckUp(8)
        turtle.refuel()
        turtle.digUp()
    end
end

function checkItemCount()
    if turtle.getItemCount() == 0 then
        turtle.select(2)
        turtle.placeUp()
        turtle.select(3)
        turtle.suckUp(1)
        turtle.select(2)
        turtle.digUp()
        turtle.select(3)
    end
end

function startTurtle()
    for i = 5, 8 do
        turtle.select(i)
        turtle.dropDown(1)
    end
end

checkFuel()
local count = 1
while turtle.forward() do
    checkFuel()

    print(count)

    if count == 1 then
        turtle.select(3)
        turtle.turnLeft()
        turtle.placeDown()
        checkItemCount()
        startTurtle()
        turtle.turnRight()
    elseif count == 16 then
        turtle.select(4)
        turtle.placeUp()

        count = 0
    end

    count = count + 1
end
